import 'package:hive_flutter/adapters.dart';
import 'package:todo_app_with_bloc_and_hivedb/model/user.dart';

class AuthenticationService {
  late Box<User> _users;

  Future<void> init() async {
    Hive.registerAdapter(UserAdapter());
    _users = await Hive.openBox<User>('usersBox');

    _users.add(User('user1', 'password1'));

    _users.add(User('user2', 'password2'));
  }

  Future<String?> authenticateUser(
      final String username, final String password) async {
    final success = await _users.values.any((element) =>
        element.username == username && element.password == password);
    if (success) {
      return username;
    } else {
      return null;
    }
  }

  Future<UserCreationResult?> createUser(
      final String username, final String password) async {
    final alreadyExists = _users.values.any(
      (element) => element.username.toLowerCase() == username.toLowerCase(),
    );

    if (alreadyExists) {
      return UserCreationResult.already_success;
    }

    try {
      _users.add(User(username, password));
      return UserCreationResult.success;
    } on Exception catch (e) {
      return UserCreationResult.failure;
    }
  }
}

enum UserCreationResult { success, failure, already_success }
