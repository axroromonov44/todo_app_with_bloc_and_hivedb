import 'package:hive_flutter/adapters.dart';
import 'package:todo_app_with_bloc_and_hivedb/model/task.dart';

class TodoService {
  late Box<Task> _tasks;

  Future<void> init() async {
    Hive.registerAdapter(TaskAdapter());
    _tasks = await Hive.openBox<Task>('tasks');
    // await _tasks.clear();
    // _tasks.add(Task('user1', 'Subscribe to Flutter channel', true));
    // _tasks.add(Task('user2', 'Comment on the video', false));
  }

  List<Task> getTasks(final String username) {
    final tasks = _tasks.values.where((element) => element.user == username);
    return tasks.toList();
  }

  void addTask(final String task, final String username) {
    _tasks.add(Task(username, task, false));
  }

  Future<void> removeTask(final String task, final String username) async {
    final taskToDelete = _tasks.values.firstWhere(
        (element) => element.task == task && element.user == username);
    await taskToDelete.delete();
  }

  Future<void> updateTask(
    final String task,
    final String username,
  ) async {
    final taskToEdit = _tasks.values.firstWhere(
        (element) => element.task == task && element.user == username);
    final index = taskToEdit.key as int;
    await _tasks.put(index, Task(username, task, !taskToEdit.completed));
  }
}
