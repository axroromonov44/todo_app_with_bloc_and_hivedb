import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:todo_app_with_bloc_and_hivedb/home/home.dart';
import 'package:todo_app_with_bloc_and_hivedb/services/authentication.dart';
import 'package:todo_app_with_bloc_and_hivedb/services/todo.dart';

import 'home/bloc/home_bloc.dart';

void main() async {
  await Hive.initFlutter();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (context) => AuthenticationService()),
        RepositoryProvider(create: (context) => TodoService())
      ],
      child: MaterialApp(
        home: LoginPage(),

      ),
    );
  }
}
