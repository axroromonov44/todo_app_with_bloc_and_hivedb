import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app_with_bloc_and_hivedb/services/todo.dart';
import 'package:todo_app_with_bloc_and_hivedb/todos/todos_bloc.dart';

class TodosPage extends StatelessWidget {
  final String username;

  const TodosPage({Key? key, required this.username}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To do App'),
      ),
      body: BlocProvider(
        create: (context) =>
            TodosBloc(RepositoryProvider.of<TodoService>(context))
              ..add(LoadTodosEvent(username)),
        child: BlocBuilder<TodosBloc, TodosState>(
          builder: (context, state) {
            if (state is TodosLoadedState) {
              return ListView(children: [
                ...state.tasks
                    .map(
                      (e) => ListTile(
                        title: Text(e.task),
                        trailing: Checkbox(
                          value: e.completed,
                          onChanged: (val) {
                            BlocProvider.of<TodosBloc>(context).add(ToggleTodoEvent(e.task));
                          },
                        ),
                      ),
                    )
                    .toList(),
                ListTile(
                  title: Text('Create new task'),
                  trailing: Icon(Icons.create),
                  onTap: () async {
                    final result = await showDialog(
                      context: context,
                      builder: (context) => Dialog(
                        child: CreateNewTask(),
                      ),
                    );
                    if (result != null) {
                      BlocProvider.of<TodosBloc>(context).add(
                        AddTodosEvent(result),
                      );
                    }
                  },
                )
              ]);
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class CreateNewTask extends StatefulWidget {
  const CreateNewTask({Key? key}) : super(key: key);

  @override
  State<CreateNewTask> createState() => _CreateNewTaskState();
}

class _CreateNewTaskState extends State<CreateNewTask> {
  final _createController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('What task do you want to create'),
        TextField(
          controller: _createController,
        ),
        SizedBox(
          height: 30,
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop(_createController.text);
            },
            child: Text('Save'))
      ],
    );
  }
}
