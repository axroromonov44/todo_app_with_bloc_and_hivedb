part of 'todos_bloc.dart';

abstract class TodosEvent extends Equatable {
  const TodosEvent();
}

class LoadTodosEvent extends TodosEvent {
  final String username;

  const LoadTodosEvent(this.username);

  @override
  List<Object?> get props => [username];
}

class AddTodosEvent extends TodosEvent {
  final String createText;

  const AddTodosEvent(this.createText);
  @override
  List<Object?> get props => [createText];
}
class ToggleTodoEvent extends TodosEvent{
  final String todoTask;

  const ToggleTodoEvent(this.todoTask);

  @override
  List<Object?> get props => [todoTask];

}